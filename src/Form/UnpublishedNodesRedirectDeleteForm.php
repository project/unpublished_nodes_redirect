<?php

namespace Drupal\unpublished_nodes_redirect\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Builds the form to delete Redirect entities.
 */
class UnpublishedNodesRedirectDeleteForm extends EntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete %name?', ['%name' => $this->entity->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.unpublished_nodes_redirect.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->entity->delete();

    $message = $this->t('Content @type: deleted @label.', [
      '@type' => $this->entity->bundle(),
      '@label' => $this->entity->label(),
    ]);
    $this->messenger()->addMessage($message);

    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
