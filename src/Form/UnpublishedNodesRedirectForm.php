<?php

namespace Drupal\unpublished_nodes_redirect\Form;

use Drupal\Core\Entity\EntityForm;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;

/**
 * Class UnpublishedNodesRedirectForm.
 */
class UnpublishedNodesRedirectForm extends EntityForm {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Creates a new UnpublishedNodesRedirectForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The messenger service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, MessengerInterface $messenger) {
    $this->entityTypeManager = $entity_type_manager;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\unpublished_nodes_redirect\Entity\UnpublishedNodesRedirectInterface $unpublished_nodes_redirect */
    $unpublished_nodes_redirect = $this->entity;

    $form['id'] = [
      '#type' => 'select',
      '#title' => $this->t('Content type'),
      '#description' => $this->t('Choose on which content type to apply the redirect'),
      '#options' => $this->getNodeTypes(),
      '#default_value' => $unpublished_nodes_redirect->id(),
      '#required' => TRUE,
    ];

    // Redirect path text input.
    $form['path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Internal redirect path'),
      '#description' => $this->t('Enter an internal redirect path.'),
      '#default_value' => $unpublished_nodes_redirect->getPath(),
      '#required' => TRUE,
    ];

    // Redirect response code.
    $form['response_code'] = [
      '#type' => 'select',
      '#title' => $this->t('Response code'),
      '#description' => $this->t('Select a HTTP Response code for the redirect.'),
      '#options' => [
        301 => $this->t('301 - Moved Permanently'),
        302 => $this->t('302 - Found'),
        307 => $this->t('307 - Temporary Redirect'),
      ],
      '#default_value' => $unpublished_nodes_redirect->getResponseCode(),
      '#required' => TRUE,
    ];

    return parent::form($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Validate that the provided redirect paths are all internal.
    if (UrlHelper::isExternal($form_state->getValue('path'))) {
      $form_state->setErrorByName('path', $this->t('The provided path needs to be internal.'));
    }

    // Check that no redirect already exists for that content type.
    $id = $form_state->getValue('id');
    if ($this->entityTypeManager->getStorage('unpublished_nodes_redirect')->load($id) && $this->entity->isNew()) {
      $form_state->setErrorByName('id', $this->t('A redirect already exists for the %content_type content type.', [
        '%content_type' => $form_state->getValue('content_type'),
      ]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\unpublished_nodes_redirect\Entity\UnpublishedNodesRedirectInterface $unpublished_nodes_redirect */
    $unpublished_nodes_redirect = $this->entity;

    $id = $form_state->getValue('id');
    $node_types = $this->getNodeTypes();
    $label = isset($node_types[$id]) ? $node_types[$id] : $id;
    $unpublished_nodes_redirect->set('label', $label);

    $status = $unpublished_nodes_redirect->save();
    switch ($status) {
      case SAVED_NEW:
        $this->messenger->addMessage($this->t('Created the %label Redirect.', [
          '%label' => $unpublished_nodes_redirect->label(),
        ]));
        break;

      default:
        $this->messenger->addMessage($this->t('Saved the %label Redirect.', [
          '%label' => $unpublished_nodes_redirect->label(),
        ]));
    }

    $form_state->setRedirectUrl($unpublished_nodes_redirect->toUrl('collection'));

    return $status;
  }

  /**
   * Helper function to get node types on the site and allow them to be altered.
   *
   * @return array
   *   An array of node types whose keys are the machine names and values are
   *   the human names.
   */
  private function getNodeTypes() {
    $node_types = $this->entityTypeManager->getStorage('node_type')->loadMultiple();

    return array_map(function ($item) {
      return $item->label();
    }, $node_types);
  }

}
