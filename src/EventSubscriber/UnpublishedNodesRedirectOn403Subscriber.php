<?php

namespace Drupal\unpublished_nodes_redirect\EventSubscriber;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\EventSubscriber\HttpExceptionSubscriberBase;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Drupal\node\Entity\Node;
use Drupal\unpublished_nodes_redirect\Utils\UnpublishedNodesRedirectUtils as Utils;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;

/**
 * Unpublished Nodes Redirect On 403 Subscriber class.
 */
class UnpublishedNodesRedirectOn403Subscriber extends HttpExceptionSubscriberBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $currentRouteMatch;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The language manager
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;


  /**
   * {@inheritdoc}
   */
  protected function getHandledFormats() {
    return ['html'];
  }

  /**
   * Create a new UnpublishedNodesRedirectOn403Subscriber object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Routing\RouteMatchInterface $current_route_match
   *   The current route match.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The current user.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, RouteMatchInterface $current_route_match, AccountProxyInterface $current_user, LanguageManagerInterface $language_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->currentRouteMatch = $current_route_match;
    $this->currentUser = $current_user;
    $this->languageManager = $language_manager;
  }


  /**
   * Fires redirects whenever a 403 meets the criteria for unpublished nodes.
   *
   * @param \Symfony\Component\HttpKernel\Event\ExceptionEvent $event
   *
   * @see Utils::checksBeforeRedirect for criteria relating to if a node
   * unpublished node should be redirected.
   */
  public function on403(ExceptionEvent $event) {
    if ($node = $event->getRequest()->attributes->get('node')) {
      if (!is_object($node)) {
        $node = $this->entityTypeManager->getStorage('node')->load($node);
      }
      $language = $this->languageManager->getCurrentLanguage();
      if ($node->hasTranslation($language->getId())) {
        $node = $node->getTranslation($language->getId());
      }

      /** @var \Drupal\node\NodeInterface $node */
      $node_type = $node->getType();
      $is_published = $node->isPublished();
      /** @var \Drupal\unpublished_nodes_redirect\Entity\UnpublishedNodesRedirectInterface $config */
      $config = $this->entityTypeManager->getStorage('unpublished_nodes_redirect')->load($node_type);
      $is_anonymous = $this->currentUser->isAnonymous();

      if ($config !== NULL) {
        // Get the redirect path for this node type.
        $response_code = $config->getResponseCode();
        $redirect_path = Url::fromUri('internal:' . $config->getPath(), ['language' => $language])->toString();

        if (Utils::checksBeforeRedirect($is_published, $is_anonymous, $redirect_path, $response_code)) {
          $metadata = CacheableMetadata::createFromObject($node)
            ->addCacheableDependency($config)
            ->addCacheTags(['rendered']);
          $response = new TrustedRedirectResponse($redirect_path, $response_code);
          $response->addCacheableDependency($metadata);
          // Set response as not cacheable, otherwise browser will cache it.
          $response->setCache(['max_age' => 0]);
          $event->setResponse($response);
        }
      }
    }
  }

}
