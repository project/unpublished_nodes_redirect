<?php

namespace Drupal\unpublished_nodes_redirect\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Redirect entities.
 */
interface UnpublishedNodesRedirectInterface extends ConfigEntityInterface {

  /**
   * Returns the redirection internal path.
   *
   * @return string
   *   The redirection internal path.
   */
  public function getPath();

  /**
   * Returns the redirection response code.
   *
   * @return int
   *   The redirection response code.
   */
  public function getResponseCode();

}
