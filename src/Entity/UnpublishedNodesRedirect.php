<?php

namespace Drupal\unpublished_nodes_redirect\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Redirect entity.
 *
 * @ConfigEntityType(
 *   id = "unpublished_nodes_redirect",
 *   label = @Translation("Redirect"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\unpublished_nodes_redirect\UnpublishedNodesRedirectListBuilder",
 *     "form" = {
 *       "add" = "Drupal\unpublished_nodes_redirect\Form\UnpublishedNodesRedirectForm",
 *       "edit" = "Drupal\unpublished_nodes_redirect\Form\UnpublishedNodesRedirectForm",
 *       "delete" = "Drupal\unpublished_nodes_redirect\Form\UnpublishedNodesRedirectDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\unpublished_nodes_redirect\UnpublishedNodesRedirectHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "unpublished_nodes_redirect",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "path" = "path",
 *     "response_code" = "response_code"
 *   },
 *   links = {
 *     "canonical" = "/admin/config/system/unpublished-nodes-redirect/{unpublished_nodes_redirect}",
 *     "add-form" = "/admin/config/system/unpublished-nodes-redirect/add",
 *     "edit-form" = "/admin/config/system/unpublished-nodes-redirect/{unpublished_nodes_redirect}/edit",
 *     "delete-form" = "/admin/config/system/unpublished-nodes-redirect/{unpublished_nodes_redirect}/delete",
 *     "collection" = "/admin/config/system/unpublished-nodes-redirect"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "path",
 *     "response_code"
 *   }
 * )
 */
class UnpublishedNodesRedirect extends ConfigEntityBase implements UnpublishedNodesRedirectInterface {

  /**
   * The Redirect ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Redirect label.
   *
   * @var string
   */
  protected $label;

  /**
   * The path to redirect to.
   *
   * @var string
   */
  protected $path;

  /**
   * The HTTP response code to use with the redirect.
   *
   * @var int
   */
  protected $response_code;

  /**
   * {@inheritdoc}
   */
  public function getPath() {
    return $this->path;
  }

  /**
   * {@inheritdoc}
   */
  public function getResponseCode() {
    return $this->response_code;
  }

}
