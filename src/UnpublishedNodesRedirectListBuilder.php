<?php

namespace Drupal\unpublished_nodes_redirect;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of Redirect entities.
 */
class UnpublishedNodesRedirectListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Label');
    $header['id'] = $this->t('Machine name');
    $header['path'] = $this->t('Redirection path');
    $header['response_code'] = $this->t('Response code');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\unpublished_nodes_redirect\Entity\UnpublishedNodesRedirectInterface $entity */
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    $row['path'] = $entity->getPath();
    $row['response_code'] = $entity->getResponseCode();

    return $row + parent::buildRow($entity);
  }

}
