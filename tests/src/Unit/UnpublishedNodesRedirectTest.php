<?php

namespace Drupal\Tests\unpublished_nodes_redirect\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\unpublished_nodes_redirect\Utils\UnpublishedNodesRedirectUtils as Utils;

/**
 * Test for Unpublished nodes redirect module.
 *
 * @group unpublished_nodes_redirect
 */
class UnpublishedNodesRedirectTest extends UnitTestCase {

  /**
   * Unpublished Nodes Redirect Utilities
   *
   * @var \Drupal\unpublished_nodes_redirect\Utils\UnpublishedNodesRedirectUtils
   */
  protected $utils;

  /**
   * Unpublished Nodes Redirect Configuration
   *
   * @var \Drupal\unpublished_nodes_redirect\Entity\UnpublishedNodesRedirectInterface
   */
  protected $config;

  /**
   * Setup tests.
   */
  protected function setUp(): void {
    $this->utils = new Utils();
    $this->utils = new Utils();
    $this->config = $this->createMock('\Drupal\unpublished_nodes_redirect\Entity\UnpublishedNodesRedirectInterface');
    $this->config->set('path', 'test/path');
    $this->config->set('response_code', 301);
    parent::setUp();
  }

  /**
   * Tests checks before redirect.
   */
  public function testChecksBeforeRedirect() {
    $response_code = $this->config->getResponseCode();
    $redirect_path = $this->config->getPath();
    $this->assertTrue(TRUE, $this->utils->checksBeforeRedirect(0, TRUE, $redirect_path, $response_code));
    $this->assertFalse(FALSE, $this->utils->checksBeforeRedirect(1, TRUE, $redirect_path, $response_code));
    $this->assertFalse(FALSE, $this->utils->checksBeforeRedirect(0, FALSE, $redirect_path, $response_code));
    $this->assertFalse(FALSE, $this->utils->checksBeforeRedirect(0, FALSE, '', $response_code));
    $this->assertFalse(FALSE, $this->utils->checksBeforeRedirect(0, FALSE, $redirect_path, 0));
  }

}
